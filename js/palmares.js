$(document).ready(function() {
    var url = new URL(window.location.href)

    var baseUrl = ''
    // for test
    // baseUrl = 'https://www.adprip.fr/'

    $('#header').load(baseUrl + 'navigation.html')

    function ajouteEspaces(nom) {
        return nom
            .replace(/d([A-Z])/g, "d'$1")
            .replace(/([a-z0-9])([A-Z])/g, '$1 $2')
            .replace('St ', 'Saint ')
            .replace('Miquelon', 'et Miquelon')
            .replace('Territoirede', 'Territoire de')
            .replace(' Et ', ' et ')
            .replace(' Caledonie', '-Calédonie')
            .replace('Fr Etranger', "Français de l'étranger")
    }

    // Palmares des departements
    var compteurDepartements, departementDB
    $.when(
        $.getJSON(baseUrl + 'archive/latest.departments.json', function(data) {
            compteurDepartements = {}
            data.forEach(function(deptObj) {
                for (deptCode in deptObj) {
                    compteurDepartements[deptCode] = parseInt(deptObj[deptCode])
                }
            })
        }),
        $.get(baseUrl + 'nb_electeurs_dpt.csv', function(data) {
            departementDB = parseElecteurs(data)
        })
    ).then(function() {
        var dptFilter = $('#departementFilter')
        departementDB.forEach(function(deptObj) {
            deptObj['soutiens'] = compteurDepartements[deptObj.dpt_code]
            deptObj.dept_nom = ajouteEspaces(deptObj.dept_nom)
            dptFilter.append(
                $('<option>', { value: deptObj.dpt_code, text: deptObj.dpt_code + ' - ' + deptObj.dept_nom })
            )
        })
        var dataForTable = departementDB.map(function(item) {
            var pourcent = 0
            if (item.nb_electeurs > 0 && item.soutiens < item.nb_electeurs) {
                pourcent = 100.0 * item.soutiens / item.nb_electeurs
            }
            return [
                item.dpt_code,
                item.dept_nom,
                pourcent.toFixed(2),
                item.soutiens,
                item.nb_electeurs,
            ]
        })
        $('#PalmaresDepartements').DataTable({
            data: dataForTable,
            deferRender: true,
            columns: [
                { title: 'Code' },
                { title: 'D&eacute;partement' },
                {
                    title: 'Pourcentage',
                    render: $.fn.dataTable.render.number(' ', ',', 2, '', ' %'),
                    className: 'text-right',
                },
                { title: 'Soutiens', render: $.fn.dataTable.render.number(' ', ',', 0), className: 'text-right' },
                {
                    title: "Nombre d'&eacute;lecteurs",
                    render: $.fn.dataTable.render.number(' ', ',', 0),
                    className: 'text-right',
                },
            ],
            order: [
                [
                    2,
                    'desc',
                ],
            ],
        })
    })

    // Palmares des villes
    var compteurCommunes, villeDB, palmaresCommunesTable
    $.when(
        $.getJSON(baseUrl + 'archive/latest.communes.json', function(data) {
            compteurCommunes = data.map(function(item) {
                item.soutiens = parseInt(item.soutiens)
                item.electeurs = parseInt(item.electeurs)
                return item
            })
        }),
        $.getJSON(baseUrl + 'carto/villes.json', function(data) {
            villeDB = {}
            data.forEach(function(item) {
                villeDB[item.insee] = item.nom
            })
        })
    ).then(function() {
        var dataForTable = compteurCommunes.map(function(item) {
            var pourcent = 0
            if (item.electeurs > 0 && item.soutiens < item.electeurs) {
                pourcent = 100.0 * item.soutiens / item.electeurs
            }
            var departement = item.insee.substr(0, 2)
            if (departement === '97' || departement === '98') {
                departement = item.insee.substr(0, 3)
            }
            return [
                departement,
                item.insee,
                villeDB[item.insee],
                pourcent.toFixed(2),
                item.soutiens,
                item.electeurs,
            ]
        })
        palmaresCommunesTable = $('#PalmaresCommunes').DataTable({
            data: dataForTable,
            deferRender: true,
            columns: [
                { title: 'D&eacute;pt.' },
                { title: 'Insee' },
                { title: 'Commune' },
                {
                    title: 'Pourcentage',
                    render: $.fn.dataTable.render.number(' ', ',', 2, '', ' %'),
                    className: 'text-right',
                },
                { title: 'Soutiens', render: $.fn.dataTable.render.number(' ', ',', 0), className: 'text-right' },
                {
                    title: "Nombre d'&eacute;lecteurs",
                    render: $.fn.dataTable.render.number(' ', ',', 0),
                    className: 'text-right',
                },
            ],
            order: [
                [
                    3,
                    'desc',
                ],
            ],
            initComplete: function(settings, json) {
                $('#loadingBox').hide()
                if (url.hash === '#communes') {
                    $('#pills-communes-tab').trigger('click')
                }
            },
        })
    })

    // Search sans accents
    $.fn.dataTableExt.ofnSearch['string'] = function(data) {
        return !data
            ? ''
            : typeof data === 'string'
              ? data
                    .replace(/\n/g, ' ')
                    .replace(/[áâàä]/g, 'a')
                    .replace(/[éêèë]/g, 'e')
                    .replace(/[íîï]/g, 'i')
                    .replace(/[óôö]/g, 'o')
                    .replace(/[úüù]/g, 'u')
                    .replace(/[ÿ]/g, 'y')
                    .replace(/ñ/g, 'n')
                    .replace(/æ/g, 'ae')
                    .replace(/œ/g, 'oe')
                    .replace(/ç/g, 'c')
              : data
    }

    // Filtres
    $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        // Pas de filtre pour la vue par departement
        if (settings.sTableId === 'PalmaresDepartements') {
            return true
        }
        var dptFilter = $('#departementFilter').val()
        var arrFilter = $('#arrondissementFilter').val()
        var min = parseInt($('#minElecteurs').val(), 10)
        var max = parseInt($('#maxElecteurs').val(), 10)
        var dataDpt = data[0]
        var dataVille = data[2]
        var dataElecteurs = parseInt(data[5]) || 0

        if (
            (isNaN(min) || dataElecteurs >= min) &&
            (isNaN(max) || dataElecteurs <= max) &&
            (!dptFilter || dataDpt === dptFilter) &&
            (arrFilter === '1' || !dataVille.includes('Arrondissement'))
        ) {
            return true
        }
        return false
    })
    // Event listener to the two range filtering inputs to redraw on input
    $('#minElecteurs, #maxElecteurs').keyup(
        _.debounce(function() {
            palmaresCommunesTable.draw()
        }, 300)
    )
    $('#departementFilter, #arrondissementFilter').change(
        _.debounce(
            function() {
                palmaresCommunesTable.draw()
            },
            300,
            { leading: true }
        )
    )
})
