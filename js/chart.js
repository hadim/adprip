'use strict';
const isIos = /ip(hone)/i.test(navigator.userAgent);
const isIpad = /ip(ad)/i.test(navigator.userAgent);
const isAndroid = /android/i.test(navigator.userAgent);

var baseUrl = baseUrl || 'https://www.adprip.fr/'

$.get(baseUrl + 'archive/history.dat', function (result) {
    $.csv.toArrays(result, {
        "separator": ";"
    }, function (err, data) {
        $.get(baseUrl + 'archive/latest.count.json?nocache', function (result) {
            data.push([Math.floor(Date.now() / 1000), result["total"]]);
        (!err) ? drawChart(data): console.log(err);
        })
    })
})

function chartTendance(chartData, menuOptions) {
    // $("#chartTendance-div").empty();
    var data = [];
    chartData.forEach(obj => {
        var dataObj = {};
        dataObj["date"] = obj["date"];
        dataObj["différence avec les soutiens nécessaires"] = obj["solde"];
        dataObj["soutiens affichés journaliers"] = obj["evolution"];
        data.push(dataObj);
    });

    var dataSerie_evolution = data.filter(obj => obj["soutiens affichés journaliers"] && obj[
            "soutiens affichés journaliers"] !==
        0);

    am4core.ready(function () {
        // Themes begin
        am4core.useTheme(am4themes_animated);

        am4core.options.autoSetClassName = true;

        // Create chart instance
        var chart = am4core.create("chartTendance-div", am4charts.XYChart);

        // menu chart
        chart.exporting.menu = new am4core.ExportMenu();
        chart.exporting.menu.items = menuOptions;

        // formatage
        chart.language.locale = am4lang_fr_FR;
        chart.language.locale["_date_day"] = "dd";
        chart.language.locale["_decimalSeparator"] = ",";
        chart.language.locale["_thousandSeparator"] = " ";

        chart.dateFormatter.dateFormat = "dd/MM/yyyy";

        /* Create axes */
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.dateFormats.setKey("day", "dd/MM");
        dateAxis.periodChangeDateFormats.setKey("day", "[bold]MM");
        dateAxis.renderer.grid.template.location = 0;

        /* Create value axis */
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.grid.template.strokeDasharray = "2,3";
        valueAxis.renderer.labels.template.fill = am4core.color("#67B7DC");
        valueAxis.fontWeight = "600";
        valueAxis.title.text = "Nombre de soutiens";

        /* Create series */
        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
        columnSeries.name = "différence avec les soutiens nécessaires";
        columnSeries.dataFields.valueY = "différence avec les soutiens nécessaires";
        columnSeries.dataFields.dateX = "date";
        columnSeries.fill = am4core.color("#61738B");
        columnSeries.fillOpacity = 0.8;
        columnSeries.strokeWidth = 0;
        columnSeries.data = data;
        columnSeries.tooltipText =
            "[#fff font-size: 15px]différence\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";
        columnSeries.tooltip.label.textAlign = "middle";

        columnSeries.columns.template.adapter.add("fill", function (fill, target) {
            if (target.dataItem && (target.dataItem.valueY < 0)) {
                return am4core.color("#a55");
            } else {
                return fill;
            }
        });

        var lineSeries = chart.series.push(new am4charts.LineSeries());
        lineSeries.id = "g2";
        lineSeries.name = "soutiens affichés journaliers";
        lineSeries.dataFields.dateX = "date";
        lineSeries.dataFields.valueY = "soutiens affichés journaliers";
        lineSeries.xAxis = dateAxis;
        lineSeries.fill = am4core.color("#67B7DC");
        lineSeries.stroke = lineSeries.fill;
        lineSeries.strokeWidth = 3;
        lineSeries.data = dataSerie_evolution;

        // Add simple bullet
        var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
        bullet.circle.stroke = am4core.color("#fff");
        bullet.circle.radius = 1;
        bullet.circle.strokeWidth = 1;
        bullet.tooltipText =
            "[#fff font-size: 15px]{name}\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]";

        if (!isIos && !isAndroid) {
            var labelBullet = lineSeries.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{soutiens affichés journaliers}";
            labelBullet.label.fill = lineSeries.stroke;
            labelBullet.label.fontSize = 12;
            labelBullet.label.fontWeight = "bold";
            labelBullet.label.dy = -10;

            labelBullet.label.adapter.add("fill", function (fill, target) {
                var values = target.dataItem.dataContext;
                return (target.dataItem && values["soutiens affichés journaliers"] < values[
                    "différence avec les soutiens nécessaires"]) ? am4core.color(
                    "#EBF9FF") : fill;
            });
        }

        // ligne tendance (regression)
        var regseries = chart.series.push(new am4charts.LineSeries());
        regseries.dataFields.valueY = "soutiens affichés journaliers";
        regseries.dataFields.dateX = "date";
        regseries.strokeWidth = 2;
        regseries.stroke = am4core.color("#FF5500");
        regseries.fill = regseries.stroke;
        regseries.name = "Tendance";
        regseries.data = data;
        regseries.plugins.push(new am4plugins_regression.Regression());

        // Add simple bullet
        var regBullet = regseries.bullets.push(new am4charts.CircleBullet());
        regBullet.circle.stroke = regseries.stroke;
        regBullet.circle.radius = 1;
        regBullet.circle.strokeWidth = 0;
        regBullet.tooltipText = "{valueY}";
        regseries.tooltip.background.cornerRadius = 20;
        regseries.tooltip.background.strokeOpacity = 0;
        regseries.tooltip.pointerOrientation = "vertical";
        regseries.tooltip.dy = -10;
        regseries.tooltip.background.fill = regseries.stroke;


        regseries.events.on("ready", function () {
            var regLabel = regseries.createChild(am4core.Label);
            regLabel.fontSize = 14;
            regLabel.fontWeight = "normal";
            regLabel.fill = am4core.color("#FF5500");
            regLabel.strokeWidth = 0;
            regLabel.fillOpacity = 1;
            regLabel.padding(0, 0, 2, 0);

            // regLabel.path = regseries.segments.getIndex(0).strokeSprite.path;
            // regLabel.text = "Tendance";

            regseries.segments.getIndex(0).strokeSprite.events.on("propertychanged", function (
                event) {
                if (event.property == "path") {
                    regLabel.path = regseries.segments.getIndex(0).strokeSprite.path;
                }
            });

            regLabel.adapter.add("text", function (text, target) {
                var currentReg = target.dataItem.values.valueY.close;
                var totalDays = moment("2020-03-12").diff(moment("2019-06-13"),
                    'days') + 1;

                if (document.getElementById('chartTendance-div').offsetHeight > 0) {
                    var estimation = Math.round(totalDays * currentReg);
                    var html =
                        'En suivant la tendance actuelle pour une estimation d\'environ <b>' +
                        chart.numberFormatter.format(Math.round(currentReg));
                    html +=
                        '</b> soutiens affichés par l\'intérieur / jour, il y aurait <b>' +
                        chart
                        .numberFormatter.format(estimation) +
                        ' </b>soutiens affichés par l\'intérieur au <b>12/03/2020</b>.<br>';
                    document.getElementById('estimation-output').innerHTML = html;
                }
                return "Tendance";
            });

        }, 1000)

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.xAxis = dateAxis;

        var scrollbarX = new am4charts.XYChartScrollbar();
        scrollbarX.series.push(lineSeries);
        chart.scrollbarX = scrollbarX;

        scrollbarX.scrollbarChart.events.on("ready", function (ev) {
            ev.target.fontSize = "14px";
            ev.target.fillOpacity = 0;
        });

        chart.legend = new am4charts.Legend();

        columnSeries.legendSettings.labelText = "[bold {color}]{name}[/]";
        lineSeries.legendSettings.labelText = "[bold {color}]{name}[/]"; // {valueY.sum}[/]
        regseries.legendSettings.labelText = "[bold {color}]{name}[/]";

        var markerTemplate = chart.legend.markers.template;
        markerTemplate.width = 20;
        markerTemplate.height = 10;

        // chart.data = data;

    }); // end am4core.ready()
}

function drawChart(data) {
    // calcul théorique pour estimer les 10 % du corps électoral nécessaire
    // soit ~ 4 717 396 personnes / 274 jours (du 13 juin 2019 au 13 mars 2020)
    // moyenne journalière à tenir d'environ 17 217 soutiens...
    var moyenne_requis = 17217;
    var menuOptions = [{
        "label": '<i class="fa fa-bars"></i>',
        "menu": [{
                "label": "Exporter les data",
                "menu": [{
                        "type": "json",
                        "label": "JSON"
                    },
                    {
                        "type": "csv",
                        "label": "CSV"
                    },
                    {
                        "type": "xlsx",
                        "label": "XLSX"
                    }
                ]
            },
            {
                "label": "Exporter une image",
                "menu": [{
                        "type": "png",
                        "label": "PNG"
                    },
                    {
                        "type": "jpg",
                        "label": "JPG"
                    },
                    {
                        "type": "svg",
                        "label": "SVG"
                    },
                ]
            }
        ]
    }];

    data = data.filter(arr => $.isNumeric(arr[1])).sort((a, b) => a[1] - b[1]);

    var dateStart = moment("2019-06-13");
    var dateEnd = moment("2020-03-12");
    var diff_indays = moment().diff(dateStart, 'days') + 1; // date de départ incluse
    var days_remaining = dateEnd.diff(moment(), 'days') + 1; // date de fin non incluse

    var latestNumber = Math.max(...data.map(arr => $.isNumeric(arr[1]) ? +arr[1] : 0).sort());
    var lastUpdate = Math.max(...data.map(arr => $.isNumeric(arr[0]) ? +arr[0] : 0).sort());
    var moyenne = latestNumber / diff_indays;

    var numberOfDays_estimation = Math.round((4717396 - latestNumber) / moyenne);
    var dateEnd_estimation = moment().add(numberOfDays_estimation, "days");

    var date_limite = moment(dateEnd_estimation).isSameOrBefore(dateEnd) ?
        `serait atteint aux alentours du <b>` + dateEnd_estimation.format("DD/MM/YYYY") :
        `<span style="color:red">ne serait pas atteint pour le <b>` + dateEnd.format("DD/MM/YYYY") + `</span>`;

    var messagePC =
        `<i>Actuellement, le seuil des 4,7 millions d'électeurs soutenants la tenue d'un référendum` +
        ` (soit 10% du corps électoral) ` + date_limite + `**</b>.</i>`;

    var messageMobile = `<i>Actuellement, le seuil des 4,7 millions d'électeurs ` + date_limite + `**</b>.</i>`;

    document.getElementById("output").innerHTML = (!isIos && !isAndroid) ? messagePC : messageMobile;

    // document.getElementById("lastUpdate").innerHTML =
    //     (moment(+lastUpdate * 1000).isValid()) ?
    //     '(mise à jour le, ' + moment(+lastUpdate * 1000).format("DD/MM/YYYY à HH:mm:ss") + ')' :
    //     'mise à jour le, non définie';

    var chartData = [];
    var day = 0,
        cumul = 0,
        visits = 0,
        solde;
    for (var i = 0, lgi = diff_indays; i < lgi; i++) {
        var date = new Date('2019-06-13').getTime() + day;
        data.forEach(arr => {
            if (moment(date).format("YYYY-MM-DD") === moment(+arr[0] * 1000).format("YYYY-MM-DD"))
                visits = +arr[1];
            solde = visits - cumul;
        });
        chartData.push({
            "date": moment(date).format("YYYY-MM-DD"),
            "visits": visits,
            "cumul_theorique": cumul,
            "solde": solde
        });

        if (i > 0) {
            chartData[i]['evolution'] = chartData[i]["visits"] - chartData[i - 1]["visits"];
        } else {
            chartData[i]['evolution'] = chartData[i]["visits"];
        }
        cumul += moyenne_requis;
        day += (24 * 60 * 60 * 1000);
    }

    var dataSerie_minimum = chartData.filter(obj => obj["cumul_theorique"]);
    var dataSerie_visits = chartData.filter(obj => obj["visits"] && obj["visits"] !== 0);
    var dataSerie_officiel = [{
        date: "2019-07-01",
        visits: 465900
    }];

    var moyenne_semaine = chartData.slice(chartData.length - 7, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    moyenne_semaine = Math.round(moyenne_semaine / 7);

    var evolutionText = (moyenne_semaine > moyenne_requis) ?
        '<span style="color: green">&#8679;</span>' :
        (moyenne_semaine === moyenne_requis) ?
        '<span style="color: orange">&#8678;&#8680;</span>' :
        '<span style="color: red">&#8681;</span>';

    chartTendance(chartData, menuOptions);

    /**
     * ---------------------------------------
     * This demo offert and was created using amCharts 4.
     *
     * For more information visit:
     * https://www.amcharts.com/
     *
     * Documentation is available at:
     * https://www.amcharts.com/docs/v4/
     * ---------------------------------------
     */
    am4core.useTheme(am4themes_animated);

    var chart = am4core.create("chartdiv", am4charts.XYChart);

    am4core.options.autoSetClassName = true;

    chart.data = chartData;

    // formatage
    chart.language.locale = am4lang_fr_FR;
    chart.language.locale["_date_day"] = "dd";
    chart.language.locale["_decimalSeparator"] = ",";
    chart.language.locale["_thousandSeparator"] = " ";

    chart.dateFormatter.dateFormat = "dd/MM/yyyy";

    // menu chart
    var menuEvolution = JSON.parse(JSON.stringify(menuOptions)); // clone menuOptions before changed
    menuEvolution[0].menu.unshift({
        "label": "Voir les statistiques",
        "type": "custom",
        options: {
            callback: callbackStats
        }
    });
    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = menuEvolution;

    function callbackStats(ev) {
        // var chartData = dataset.data;

        var duration = chartData.length;

        var evolution_semaine = chartData.slice(chartData.length - 7, chartData.length)
            .map(obj => '<li><b>' + moment(obj["date"]).format("DD/MM/YYYY") +
                '</b>...&emsp;' + chart.numberFormatter.format(obj["evolution"]) + '</li>');


        var evolution = chartData[chartData.length - 1]["evolution"];

        var content = '<div style="text-align: left"><ul>'
        content += '<li><b>jours écoulés</b>...&emsp;' + duration + '</li>';
        content += '<li><b>jours restants</b>...&emsp;' + days_remaining + ' (à ce rythme)</li><br>' +
            '(en nombre de soutiens)';
        content += '<li><b>moyenne générale</b>...&emsp;' + chart.numberFormatter.format(Math.round(moyenne)) +
            '</li><br>';

        content += '<li><b>evolution 7 derniers jours</b></li>';
        content += evolution_semaine.join('\n')
        content += '</ul></div>'
        if (ev !== undefined) {
            chart.closeAllPopups();
            var popup = chart.openPopup(content);
            popup.top = 100;
            popup.left = 105;
        }
    }

    // chart
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.dateFormats.setKey("day", "dd/MM");
    dateAxis.periodChangeDateFormats.setKey("day", "[bold]MM");
    dateAxis.renderer.grid.template.location = 0;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.max = latestNumber * 1.05;
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.grid.template.strokeDasharray = "1,0";
    valueAxis.renderer.labels.template.fill = am4core.color("#67B7DC");
    valueAxis.fontWeight = "400";
    if (!isIos && !isAndroid) {
        valueAxis.title.text = "Nombre de soutiens";
    }
    // valueAxis.title.fill = "red";

    var series = chart.series.push(new am4charts.LineSeries());
    series.name = "Soutiens nécessaires";
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "cumul_theorique";
    series.tooltipText = "{valueY.value}";
    series.fill = am4core.color("#e59165");
    series.stroke = am4core.color("#e59165");
    series.strokeOpacity = 0.6;
    series.strokeWidth = 4;
    series.strokeDasharray = 4;
    series.data = dataSerie_minimum;

    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.id = "g2";
    series2.name = "Soutiens affichés par l'intérieur";
    series2.dataFields.dateX = "date";
    series2.dataFields.valueY = "visits";
    series2.xAxis = dateAxis;
    series2.fill = am4core.color("#67B7DC");
    series2.stroke = series2.fill;
    series2.strokeWidth = 3;
    series2.data = dataSerie_visits;

    var series3 = chart.series.push(new am4charts.LineSeries());
    series3.name = "Soutiens validés (chiffre officiel)";
    series3.dataFields.dateX = "date";
    series3.dataFields.valueY = "visits";
    series3.xAxis = dateAxis;
    series3.fill = am4core.color("#808000");
    series3.stroke = series3.fill;
    series3.strokeWidth = 3;
    series3.data = dataSerie_officiel;

    // Add simple bullet
    var bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.stroke = am4core.color("#fff");
    bullet.circle.radius = 3;
    bullet.circle.strokeWidth = 2;
    // bullet.tooltipText = "Value: [bold]{visits}[/]";

    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{cumul_theorique}";
    // labelBullet.label.fill = am4core.color("#e59165");
    labelBullet.label.fontSize = "14px";
    labelBullet.label.dy = -12;
    labelBullet.label.dx = 0;

    // Add simple bullet
    var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
    bullet2.circle.stroke = am4core.color("#fff");
    bullet2.circle.radius = 3;
    bullet2.circle.strokeWidth = 2;
    // bullet2.tooltipText = "Value: [bold]{visits}[/]";

    var labelBullet2 = series2.bullets.push(new am4charts.LabelBullet());
    labelBullet2.label.text = "{visits}";
    labelBullet2.label.fontSize = "14px";
    labelBullet2.label.fontWeight = "bold";
    labelBullet2.label.dy = -5;
    labelBullet2.label.dx = -30;

    // Add simple bullet
    var bullet3 = series3.bullets.push(new am4charts.CircleBullet());
    bullet3.circle.stroke = series3.stroke;
    bullet3.circle.radius = 6;
    bullet3.circle.strokeWidth = 3;
    bullet3.tooltipText = "{valueY}";
    series3.tooltip.background.cornerRadius = 20;
    series3.tooltip.background.strokeOpacity = 0;
    series3.tooltip.pointerOrientation = "vertical";
    series3.tooltip.dy = -10;
    series3.tooltip.background.fill = series3.stroke;

    var labelBullet3 = series3.bullets.push(new am4charts.LabelBullet());
    labelBullet3.label.text = "{visits}";
    labelBullet3.label.fontSize = "14px";
    labelBullet3.label.fontWeight = "bold";
    labelBullet3.label.dy = -5;
    labelBullet3.label.dx = -30;

    // construction tooltip html sur courbe "soutiens affichés par l'intérieur"
    series2.tooltipHTML = `<div style="border: 1px solid rgba(255,255,255,0.5); border-radius:5px">au {dateX}</div>
    <table>
    <tr>
      <th align="left">Soutiens : </th>
      <td>{valueY.value}</td>
    </tr>
    <tr>
    <th align="left"> Différence : </th>
      <td> {solde}</td>
    </tr>
    </table>`;

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.xAxis = dateAxis;

    var scrollbarX = new am4charts.XYChartScrollbar();
    scrollbarX.series.push(series2);
    chart.scrollbarX = scrollbarX;

    scrollbarX.scrollbarChart.events.on("ready", function (ev) {
        ev.target.fontSize = "14px";
        ev.target.fillOpacity = 0;
    });

    // if (isIos || isAndroid) {
    var last = series.data.length - 1;
    var middle = Math.floor(last / 2);
    labelBullet.label.adapter.add("fillOpacity", (fillOpacity, target) => {
        var index = target.dataItem.index;
        return (index === 0) ? 1 : (index === middle) ? 1 : (index === last) ? 1 : 0;
    });
    labelBullet.label.adapter.add("dx", (dx, target) => {
        var index = target.dataItem.index;
        // console.log(target.dataItem)
        return (index === 0) ? 15 : (index === last) ? -20 : 0;
    });
    var last2 = series2.data.length - 1;
    var middle2 = Math.floor(last2 / 2);
    labelBullet2.label.adapter.add("fillOpacity", function (fillOpacity, target) {
        var index = target.dataItem.index;
        return (index === 0) ? 1 : (index === middle2) ? 1 : (index === last2) ? 1 : 0;
    });
    // }

    // la grille presque invisible permet d'alléger le graphique déjà bien chargé
    // ça permet aussi de mieux voir les prolongements hachurés au survol avec XYCursor
    valueAxis.renderer.grid.template.strokeOpacity = 0.08;
    dateAxis.renderer.grid.template.strokeOpacity = 0.08;

    chart.legend = new am4charts.Legend();

    series.legendSettings.labelText = "[bold {color}]{name}[/]";
    series2.legendSettings.labelText = "[bold {color}]{name}[/]";

    var tendance_arrow = (moyenne_semaine > moyenne_requis) ?
        '<span style="color: green">&#8679;</span>' :
        (moyenne_semaine === moyenne_requis) ?
        '<span style="color: orange">&#8678;&#8680;</span>' :
        '<span style="color: red">&#8681;</span>';

    var button_tendance = (moyenne_semaine > moyenne_requis) ?
        '<button id="tendance-btn" type="button" class="btn btn-success btn-sm" title="tendance et estimation">Tendance</button>' :
        (moyenne_semaine === moyenne_requis) ?
        '<button id="tendance-btn" type="button" class="btn btn-warning btn-sm" title="tendance et estimation">Tendance</button>' :
        '<button id="tendance-btn" type="button" class="btn btn-danger btn-sm" title="tendance et estimation">Tendance</button>';

    var contentTendance = '<div style="margin-bottom: 1em;">' + tendance_arrow + '&emsp;' + button_tendance +
        '&emsp;' + tendance_arrow + '</div>';
    contentTendance +=
        '<div id="soutiens_valides" class="d-inline-block"><span class="badge badge-secondary" style="font-size: 14px">';
    contentTendance += chart.numberFormatter.format(latestNumber) +
        '</span> soutiens affichés par l\'intérieur* &emsp;</div>';
    contentTendance +=
        '<div id="minimum_requis" class="d-inline-block"><span class="badge badge-secondary" style="font-size: 14px">';
    contentTendance += chart.numberFormatter.format(chartData.map(obj => obj["cumul_theorique"]).slice(-1)[0]) +
        '</span> soutiens nécessaires&emsp;</div>';
    contentTendance +=
        '<div id="moyenne_semaine" class="d-inline-block"><span class="badge badge-secondary" style="font-size: 14px">';
    contentTendance += chart.numberFormatter.format(moyenne_semaine) +
        '</span> soutiens par jour sur semaine en cours</div>';

    document.getElementById('tendance-div').innerHTML = contentTendance;

    document.getElementById('tendance-btn').onclick = (ev) => {
        if (ev.target.innerHTML === "Tendance") {
            $('#chartdiv').hide();
            $('#chartTendance-div').fadeIn(1000);
            document.getElementById('mainTitle').innerHTML = 'Tendance & Estimation';
            $('#output').hide();
            $('#estimation-output').show();
            ev.target.innerHTML = "Evolution";
        } else {
            $('#chartTendance-div').hide();
            $('#chartdiv').fadeIn(1000);
            $("#tendance-chart").hide();
            $("#evolution-chart").show();
            document.getElementById('mainTitle').innerHTML = 'Évolution du nombre de soutiens';
            $('#estimation-output').hide();
            $('#output').show();
            ev.target.innerHTML = "Tendance";
        }
    };

    $("#mainloader").hide();
    $("#maindiv").show();
    console.log('Hello adprip !');
}

$(() => {
    $('#header').load(baseUrl + 'navigation.html');
    document.getElementById('main-container').style.display = "block";

    document.getElementById('guide-btn').onclick = function (e) {
        
        e.preventDefault // don't change url no click

        var offsetHeigth = document.getElementById('chartTendance-div').offsetHeight;
        var intro = introJs();
        var elementTendance = {
            element: `#estimation-output`,
            intro: `<p>Estimation du nombre de soutiens qui seront totalisés à la date butoir du 12/03/2010
            à partir d'une projection de la tendance du nombre de soutiens affichés journalièrement par le ministère de l’Intérieur</p>`,
            position: `top`
        };
        var options = [{
                intro: `<h4>Bienvenue dans la visite guidée.</h4><p>Vous pouvez interrompre la visite à tout moment
                        et y revenir quand vous voulez en cliquant sur "Visite guidée"</p>`
            },
            {
                element: `#soutiens_valides`,
                intro: `Nombre en valeurs cumulées des soutiens affichés journalièrement
                    par le site du ministère de l’Intérieur.`,
                position: 'bottom'
            },
            {
                element: `#minimum_requis`,
                intro: `Nombre en valeurs cumulées des soutiens nécessaires
                    pour atteindre le "seuil minimum requis" d’environ 4,7 millions de soutiens, soit 10% du corps électoral`,
                position: `bottom`
            },
            {
                element: `#moyenne_semaine`,
                intro: `"Moyenne journalière réalisée au cours des 7 derniers jours pour
                    les soutiens affichés journalièrement par le site du ministère de l’Intérieur`,
                position: 'bottom'
            },
            {
                element: `#output`,
                intro: `<p>Estimation d'une date de fin "virtuelle" calculée à partir des soutiens déjà validés
                    et du nombre actualisé de jours restants qu'il faudrait pour obtenir le nombre de soutiens nécessaires.</p>
                    <p>Si la moyenne des soutiens validés est trop basse, le "seuil minimum requis" d’environ 4,7 millions de soutiens,
                    soit 10% du corps électoral pourrait ne pas être atteint pour le 12/03/2019 date butoir.
                    <a href="https://www.referendum.interieur.gouv.fr/" rel="nofollow" target="_blank"
                    	title="referendum.interieur.gouv.fr">En savoir plus</a></p>`,
                position: `top`
            },
            {
                element: `#tendance-btn`,
                intro: `Permet de passer d'un graphique à l'autre :
                    <ul><li>évolution vers tendance</li><li>tendance vers évolution</li></ul>`,
                position: `top`
            }
        ];
        if (offsetHeigth > 0)
            options.splice(4, 1, elementTendance);

        var optionsTendance =
            intro.setOptions({
                tooltipPosition: 'top',
                nextLabel: 'suivant',
                prevLabel: 'retour',
                skipLabel: 'sortir',
                doneLabel: 'ok',
                showProgress: true,
                showStepNumbers: true,
                steps: options
            });
        intro.start();
    }
});
